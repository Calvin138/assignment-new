package forum.student.assignment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;
    ArrayList<Data> datas;

    public MyAdapter(Context c, ArrayList<Data> d){
        context = c;
        datas = d;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.datess.setText("On " + datas.get(i).getDates());
        myViewHolder.weightss.setText("Your weight is " + datas.get(i).getWeights() + "KG");
        myViewHolder.heightss.setText("You are " + datas.get(i).getHeights() + " CM tall");
        myViewHolder.bmiss.setText("And your BMI is " + datas.get(i).getBMI());

    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView datess, weightss, heightss, bmiss;
        public MyViewHolder(View itemView){
            super(itemView);
            datess = (TextView)itemView.findViewById(R.id.tv_date1);
            weightss = (TextView)itemView.findViewById(R.id.tv_weight1);
            heightss = (TextView)itemView.findViewById(R.id.tv_height1);
            bmiss = (TextView)itemView.findViewById(R.id.tv_bmi);


        }
    }

}
