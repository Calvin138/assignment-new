package forum.student.assignment;

public class UserData {
    public String userEmail, userName, uid;

    public UserData(String userEmail, String userName, String uid) {
        this.userEmail = userEmail;
        this.userName = userName;
        this.uid = uid;
    }

    public UserData() {
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}

