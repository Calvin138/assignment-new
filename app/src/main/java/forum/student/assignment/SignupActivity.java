package forum.student.assignment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {

    private EditText name, email, password;
    private Button signup;
    String username, useremail, uid, userpassword;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setupUI();

        firebaseAuth = FirebaseAuth.getInstance();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    username = name.getText().toString();
                    useremail = email.getText().toString();
                    userpassword = password.getText().toString();

                    firebaseAuth.createUserWithEmailAndPassword(useremail, userpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                sendUserData();
                                Toast.makeText(SignupActivity.this, "Registration successfull", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                                firebaseAuth.signOut();
                            }else{
                                Toast.makeText(SignupActivity.this, "Registration fail", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private Boolean validate(){
        Boolean result = false;
        username = name.getText().toString();
        useremail = email.getText().toString();
        userpassword = password.getText().toString();

        if(username.isEmpty() || useremail.isEmpty() || userpassword.isEmpty()){
            Toast.makeText(SignupActivity.this, "Fill in all the detail",Toast.LENGTH_SHORT).show();
        }
        else{
            return true;
        }
        return result;
    }
    private void sendUserData() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = firebaseDatabase.getReference().child("Users").child(firebaseAuth.getCurrentUser().getUid());
        uid = firebaseAuth.getCurrentUser().getUid();
        UserData userData = new UserData(useremail, username, uid);
        myRef.setValue(userData);

    }

    public void setupUI(){
        name = (EditText) findViewById(R.id.et_name);
        email = (EditText) findViewById(R.id.et_em);
        password = (EditText) findViewById(R.id.et_pw);
        signup = (Button) findViewById(R.id.btn_register);
    }
}
