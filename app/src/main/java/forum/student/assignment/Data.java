package forum.student.assignment;

import java.util.Date;

public class Data {
    private String dates;
    private String weights;
    private String heights;
    private String BMI;

    public Data() {
    }

    public Data(String dates, String weights, String heights, String BMI) {
        this.dates = dates;
        this.weights = weights;
        this.heights = heights;
        this.BMI = BMI;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getWeights() {
        return weights;
    }

    public void setWeights(String weights) {
        this.weights = weights;
    }

    public String getHeights() {
        return heights;
    }

    public void setHeights(String heights) {
        this.heights = heights;
    }

    public String getBMI() {
        return BMI;
    }

    public void setBMI(String BMI) {
        this.BMI = BMI;
    }

}