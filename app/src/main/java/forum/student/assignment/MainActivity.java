package forum.student.assignment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference weightref;
    public String saveCurrentDate;
    public Button signout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        weightref = FirebaseDatabase.getInstance().getReference("ALL BMI");

        signout = (Button)findViewById(R.id.btn_signout);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

    }

        public void onButtonClick(View view){
            switch (view.getId()){
                case R.id.Calculate:
                    CalculateBMI();
                    break;
                case R.id.btn_calculate:
                    calculateTargetWeight();
                    break;
                case R.id.btn_ToLogin:
                    toLogin();
                    break;
            }

        }

        public void toLogin(){

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            if(user == null){
                startActivity(new Intent(this, LoginActivity.class));
            }else if(user != null){
                startActivity(new Intent(this, PastResult.class));
            }
        }

        public double calculateBMI(double weight, double height){
            return (double) ((weight)  / ((height /100) * (height/100)));
        }

        public void CalculateBMI() {

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            if (user != null) {
                EditText weightNum = (EditText) findViewById(R.id.et_Weight);
                EditText heightNum = (EditText) findViewById(R.id.et_Height);
                TextView Yourbmi = (TextView) findViewById(R.id.yourBMI);
                EditText idealbmi = (EditText) findViewById(R.id.IdealBMI);
                TextView idea = (TextView) findViewById(R.id.Ideall);
                String userName;

                Calendar calFordDate = Calendar.getInstance();
                SimpleDateFormat currentDate = new SimpleDateFormat("dd-MMMM-yyyy");
                saveCurrentDate = currentDate.format(calFordDate.getTime());

                double weight;
                weight = 0;

                double height;
                height = 0;

                if (!(weightNum.getText().toString().equals(""))) {
                    weight = Double.parseDouble(weightNum.getText().toString());
                }

                if (!(heightNum.getText().toString().equals(""))) {
                    height = Double.parseDouble(heightNum.getText().toString());
                }

                double bmi;
                bmi = calculateBMI(weight, height);
                DecimalFormat x = new DecimalFormat("##.#");

                double weights;
                weights = weight;

                double heights;
                heights = height;

                String datetoday;
                datetoday = saveCurrentDate;

                if (bmi < 18.5) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are underweight!!!, Your Ideal BMI is 19 - 24");
                } else if (bmi >= 18.5 && bmi <= 24.9) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are in perfect shape, this BMI is ideal.");
                } else if (bmi >= 25 && bmi <= 29.9) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are overweight, Your Ideal BMI is 19 - 24!!!");
                } else if (bmi < 30) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are considered obese, Your Ideal BMI is 19 - 24!!!");
                }

                HashMap postMap = new HashMap();
                postMap.put("dates", saveCurrentDate);
                postMap.put("weights", x.format(weights));
                postMap.put("heights", x.format(heights));
                postMap.put("BMI", x.format(bmi));
                postMap.put("uid", FirebaseAuth.getInstance().getCurrentUser().getUid());

                weightref.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(saveCurrentDate).updateChildren(postMap);
            }
            else {
                EditText weightNum = (EditText) findViewById(R.id.et_Weight);
                EditText heightNum = (EditText) findViewById(R.id.et_Height);
                TextView Yourbmi = (TextView) findViewById(R.id.yourBMI);
                EditText idealbmi = (EditText) findViewById(R.id.IdealBMI);
                TextView idea = (TextView) findViewById(R.id.Ideall);
                String userName;

                Calendar calFordDate = Calendar.getInstance();
                SimpleDateFormat currentDate = new SimpleDateFormat("dd-MMMM-yyyy");
                saveCurrentDate = currentDate.format(calFordDate.getTime());

                double weight;
                weight = 0;

                double height;
                height = 0;

                if (!(weightNum.getText().toString().equals(""))) {
                    weight = Double.parseDouble(weightNum.getText().toString());
                }

                if (!(heightNum.getText().toString().equals(""))) {
                    height = Double.parseDouble(heightNum.getText().toString());
                }

                double bmi;
                bmi = calculateBMI(weight, height);
                DecimalFormat x = new DecimalFormat("##.#");

                double weights;
                weights = weight;

                double heights;
                heights = height;

                String datetoday;
                datetoday = saveCurrentDate;

                if (bmi < 18.5) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are underweight!!!, Your Ideal BMI is 19 - 24");
                } else if (bmi >= 18.5 && bmi <= 24.9) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are in perfect shape, this BMI is ideal.");
                } else if (bmi >= 25 && bmi <= 29.9) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are overweight, Your Ideal BMI is 19 - 24!!!");
                } else if (bmi < 30) {
                    Yourbmi.setText("Your BMI is = " + x.format(bmi) + ", You are considered obese, Your Ideal BMI is 19 - 24!!!");
                }

                Toast.makeText(this, "Your data is not saved, Login to save your data", Toast.LENGTH_SHORT).show();

            }
        }

        public void calculateTargetWeight(){
            EditText weightNum = (EditText)findViewById(R.id.et_Weight);
            EditText heightNum = (EditText)findViewById(R.id.et_Height);
            TextView Yourbmi = (TextView)findViewById(R.id.yourBMI);
            EditText idealbmi = (EditText)findViewById(R.id.IdealBMI);
            TextView idea = (TextView)findViewById(R.id.Ideall);

            double weight;
            weight = 0;

            double height;
            height = 0;

            double calories;
            calories = 250;

            if (!(weightNum.getText().toString().equals(""))) {
                weight = Double.parseDouble(weightNum.getText().toString());
            }

            if (!(heightNum.getText().toString().equals(""))) {
                height = Double.parseDouble(heightNum.getText().toString());
            }
            double bmi;
            bmi = calculateBMI(weight, height);
            DecimalFormat x = new DecimalFormat("##.#");

            double y, ideal, kg, kg1;
            ideal = 0;
            y = 0;
            kg = 0;
            kg1 = 0;

            if(!(idealbmi.getText().toString().equals(""))){
                ideal = Double.parseDouble(idealbmi.getText().toString());
            }

            if(ideal < weight){
                y = weight - ideal;
                kg = calories * y;
                kg1 = kg / 30;
                idea.setText("To reach your targeted weight in 1 month, you have to lose " + y + " KG," + " you have to reduce your calories intake by " + x.format(kg1) + " calories per day");
            }
            else if (ideal > weight) {
                y = ideal - weight;
                kg = calories * y;
                kg1 = kg / 30;
                idea.setText("To reach your targeted weight in 1 month, you have to gain " + y + " KG," + " you have to increase your calories intake by " + x.format(kg1) + " calories per day");
            }



        }



}
