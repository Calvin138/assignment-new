package forum.student.assignment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PastResult extends AppCompatActivity {

    DatabaseReference reference, nameref;
    RecyclerView recyclerView;
    ArrayList<Data> list;
    MyAdapter adapter;
    private Button main;
    private TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_result);

        main = (Button)findViewById(R.id.btn_main);
        recyclerView = (RecyclerView)findViewById(R.id.PPA1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        userName = (TextView)findViewById(R.id.tv_name);

        list = new ArrayList<Data>();

        reference = FirebaseDatabase.getInstance().getReference().child("ALL BMI").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren())
                {
                    Data d = dataSnapshot1.getValue(Data.class);
                    list.add(d);

                }
                adapter = new MyAdapter(PastResult.this, list);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(PastResult.this, "Some thing is wrong", Toast.LENGTH_SHORT).show();

            }
        });

        nameref = FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        nameref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserData userData = dataSnapshot.getValue(UserData.class);
                userName.setText("HI " + userData.getUserName() + " This is your past result.");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(PastResult.this, "Some thing is wrong", Toast.LENGTH_SHORT).show();
            }
        });

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PastResult.this, MainActivity.class));
            }
        });

    }
}
